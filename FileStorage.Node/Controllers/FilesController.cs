﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using FileStorage.Data.Helpers;
using FileStorage.Data.Models;
using FileStorage.Node.Extensions;
using FileStorage.Node.Services;

namespace FileStorage.Node.Controllers
{
    //[Authorize]
    [AllowAnonymous]
    public class FilesController : ApiController
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly ManagerClientService _managerClient = new ManagerClientService();
        // GET api/Files/{0f8fad5b-d9cb-469f-a165-70867728950e}
        //[System.Web.Http.AcceptVerbs("GET", "POST")]
        //[System.Web.Http.HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            //Logger.Info("Get started");
            //Request to File storage manager to get file details and check user access rights

            TransactionInformation<FileLocationDetailResponse> result = _managerClient.GetFileLocationDetail(id, false);
            if (!result.Success)
            {
                return Content(result.StatusCode, String.Join(";", result.ErrorMessages));
            }

            return new FileActionResult(result.Result, id);

            //Read file
            /*var localFilePath = HttpContext.Current.Server.MapPath("~/timetable.jpg");

            if (!File.Exists(localFilePath))
            {
                result = Request.CreateResponse(HttpStatusCode.Gone);
            }
            else
            {// serve the file to the client
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = "SampleImg";
            }

            
            if (!_context.Servers.Any(t => t.Key == "Key2"))
            {
                BadRequest("Server not authorised");
            }
            if (!_context.TemporaryAuthorizations.Any(t => t.FileId == key))
            {
                BadRequest("User does not have access to requested file");
            }

            AuthorizationRequestReadDTO result =
                _context.Files.Where(t => t.Id == key).Select(t => new AuthorizationRequestReadDTO()
                {
                    FileName = t.Name,
                    FileExtension = t.Extension,
                    ContainerName = t.Container.Name,
                    Path = t.Container.FilesLocation.Path
                }).FirstOrDefault();
            if (result == null)
            {
                NotFound();
            }

            return Ok(result);*/
        }

        /*// GET api/values/5
        public string Get(int id)
        {
            return "value";
        }*/

        

        [ValidateMimeMultipartContentFilter]
        // POST api/Files/{0f8fad5b-d9cb-469f-a165-70867728950e}
        public IHttpActionResult Post()
        {
            //Request to File storage manager to get file details and check user access rights
            /*string nodeKey = ConfigurationManager.AppSettings["NodeKey"];
            string managerUrl = ConfigurationManager.AppSettings["ManagerUrl"];
            FileLocationDetailResponse fileDetails;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(managerUrl + "/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.
                HttpResponseMessage response = client.GetAsync("FileOperations?id=" + id + "&login=kserks&nodeKey=" + nodeKey + "&toWrite=False").Result;
                Logger.Info("Get started");
                if (response.IsSuccessStatusCode)
                {
                    fileDetails = response.Content.ReadAsAsync<FileLocationDetailResponse>().Result;
                    Logger.Info("{0}\t${1}\t{2}", fileDetails.Path + "\\" + fileDetails.ContainerName, fileDetails.FileName, fileDetails.FileExtension);
                }
                else
                {
                    var errorMessage = response.Content.ReadAsStringAsync().Result;
                    Logger.Error("Cannot get data from " + managerUrl + ". Error: " + response.StatusCode + " " + errorMessage);
                    return BadRequest(response.ReasonPhrase + ", " + errorMessage);
                }
            }*/

            //Only single file upload supported
            int filesPassed = HttpContext.Current.Request.Files.Count;
            if (filesPassed == 0)
                return Content(HttpStatusCode.BadRequest, "File not found in request. Please send file");
            /*if (filesPassed > 1)
                return Content(HttpStatusCode.BadRequest, filesPassed + " files found in request. Please send only 1 file");*/

            List<FileUploadResult> results = new List<FileUploadResult>();
            //Get data from request
            string[] fileIds = HttpContext.Current.Request.Form.GetValues("fileId");
            if (fileIds == null || fileIds.Count() != filesPassed)
                return Content(HttpStatusCode.BadRequest, "Number of file Ids " + fileIds + " does not match number of files " + filesPassed);
            int iteration = 0;
            foreach (string fileField in HttpContext.Current.Request.Files)
            {
                string fileId = fileIds[iteration];
                iteration++;
                FileUploadResult result = new FileUploadResult(fileField);
                Guid id;
                if (fileId == null)
                {
                    result.Success = false;
                    result.ErrorMessage = "File id is null";
                    results.Add(result);
                    continue;
                }

                if (fileField == null)
                {
                    result.Success = false;
                    result.ErrorMessage = "File is null";
                    results.Add(result);
                    continue;
                }

                try
                {
                    id = Guid.Parse(fileId);
                }
                catch (FormatException ex)
                {
                    result.Success = false;
                    result.ErrorMessage = "Cannot parse name (file id): " + ex.Message;
                    results.Add(result);
                    continue;
                }

                //Logger.Info("File Id: " + fileId);
                //HttpPostedFileBase file = HttpContext.Current.Request.Files[fileId];

                TransactionInformation<FileLocationDetailResponse> fileLocationDetailRequest =
                    _managerClient.GetFileLocationDetail(id, true);
                if (!fileLocationDetailRequest.Success)
                {
                    result.Success = false;
                    result.ErrorMessage = string.Join(";", fileLocationDetailRequest.ErrorMessages);
                    results.Add(result);
                    continue;
                }

                string fileNameWithPath = FileHelper.FullPathCalculator(fileLocationDetailRequest.Result, id);

                

                //Get file from request
                HttpPostedFile httpPostedFile = HttpContext.Current.Request.Files[fileField];
                if (httpPostedFile == null)
                {
                    result.Success = false;
                    result.ErrorMessage = "Cannot get file from request";
                    results.Add(result);
                    continue;
                }
                if (File.Exists(fileNameWithPath))
                {
                    result.Success = false;
                    result.ErrorMessage = "File already exists. To override file use PUT method";
                    results.Add(result);
                    continue;
                }
                if (httpPostedFile.ContentLength != fileLocationDetailRequest.Result.Size)
                {
                    result.Success = false;
                    result.ErrorMessage = "File size does not match. Uploaded file size: " + httpPostedFile.ContentLength + ", expected file size: " + fileLocationDetailRequest.Result.Size;
                    results.Add(result);
                    //Logger.Info("File size: " + httpPostedFile.ContentLength + ", expected file size: " + fileLocationDetailRequest.Result.Size);
                    continue;
                }

                // Save the uploaded file
                httpPostedFile.SaveAs(fileNameWithPath);

                //Validate file
                string fileHash;
                /*DateTime startCalculation;
                DateTime endCalculation;
                string fileHashMd5;
                //Test
                using (var stream = File.OpenRead(fileNameWithPath))
                {
                    startCalculation = DateTime.Now;
                    using (var md5 = MD5.Create())
                    {
                        fileHashMd5 = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                    }
                    endCalculation = DateTime.Now;
                    Logger.Info("test Hash md5 for file " + id + ": " + fileHashMd5 + ", time " + (endCalculation - startCalculation));
                }

                using (var stream = File.OpenRead(fileNameWithPath))
                {
                    startCalculation = DateTime.Now;
                    xxHash hash = new xxHash();
                    fileHash = BitConverter.ToString(hash.ComputeHash(stream)).Replace("-", "").ToLower();
                    endCalculation = DateTime.Now;
                    Logger.Info("test Hash xxHash for file " + id + ": " + fileHash + ", time " +
                                (endCalculation - startCalculation));
                }

                //byte[] input = Encoding.UTF8.GetBytes("hello world");
                //Console.WriteLine("{0:X}", xxHash.CalculateHash(input));
                using (var stream = File.OpenRead(fileNameWithPath))
                {
                    startCalculation = DateTime.Now;
                    xxHash hash = new xxHash();
                    fileHash = BitConverter.ToString(hash.ComputeHash(stream)).Replace("-", "").ToLower();
                    endCalculation = DateTime.Now;
                    Logger.Info("Hash xxHash for file " + id + ": " + fileHash + ", time " + (endCalculation - startCalculation));
                    
                    startCalculation = DateTime.Now;
                    using (var md5 = MD5.Create())
                    {
                        fileHash = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                    }
                    endCalculation = DateTime.Now;
                    Logger.Info("Hash md5 for file " + id + ": " + fileHashMd5 + ", time " + (endCalculation - startCalculation));

                    if (fileLocationDetailRequest.Result.Hash == fileHash || fileLocationDetailRequest.Result.Hash == null)
                    {
                        //File successfuly loaded
                        //Send success status to FileSorage Manager
                        //string fileName = Path.GetExtension(httpPostedFile.FileName);
                        //string fileExtension = Path.GetFileNameWithoutExtension(httpPostedFile.FileName);
                        OperationResult fileUploadSuccessRequest = _managerClient.SendUploadSuccessResult(id, fileLocationDetailRequest.Result.Hash == null ? fileHash : null);
                        if (fileUploadSuccessRequest.Success)
                        {
                            results.Add(result);
                            continue;
                        }

                        result.Success = false;
                        result.ErrorMessage = "File was saved on the server, but error during success upload confirmation. " +
                            string.Join(";", fileUploadSuccessRequest.ErrorMessages);
                        results.Add(result);
                        continue;
                    }
                }*/


                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(fileNameWithPath))
                    {
                        fileHash = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
                        //Logger.Info("Hash for file " + id + ": " + fileHash);
                        if (fileLocationDetailRequest.Result.Hash == fileHash || fileLocationDetailRequest.Result.Hash == null)
                        {
                            //File successfuly loaded
                            //Send success status to FileSorage Manager
                            TransactionInformation fileUploadSuccessRequest = _managerClient.SendUploadSuccessResult(id, fileLocationDetailRequest.Result.Hash == null ? fileHash : null);
                            if (fileUploadSuccessRequest.Success)
                            {
                                results.Add(result);
                                continue;
                            }

                            result.Success = false;
                            result.ErrorMessage = "File was saved on the server, but error during success upload confirmation. " +
                                string.Join(";", fileUploadSuccessRequest.ErrorMessages);
                            results.Add(result);
                            continue;
                        }
                    }
                }
                //File loaded with errors, going to delete file
                File.Delete(fileNameWithPath);
                //Do not send status to FileSorage Manager (file will be deleted after download experation time without upload)
                result.Success = false;
                result.ErrorMessage = "Error during file transfer. Hash " + fileHash + " is not valid. Please try to repeat upload";
                results.Add(result);
            }

            if (results.Any(t => !t.Success))
            {
                //If at least 1 error then return bad request
                return Content(HttpStatusCode.BadRequest, results);
            }

            return Ok(results);
        }

        /*// PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }*/
    }
}
