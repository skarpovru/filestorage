﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.IO;
using FileStorage.Data.Helpers;
using FileStorage.Data.Models;

namespace FileStorage.Node.Services
{   
    public class FileActionResult : IHttpActionResult
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public FileLocationDetailResponse FileLocationDetail { get; private set; }
        public Guid FileId { get; private set; }
        public FileActionResult(FileLocationDetailResponse fileLocationDetail, Guid fileId)
        {
            FileLocationDetail = fileLocationDetail;
            FileId = fileId;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string fileNameWithPath = FileHelper.FullPathCalculator(FileLocationDetail, FileId);
            Logger.Info("File: " + fileNameWithPath);
            response.Content = new StreamContent(File.OpenRead(fileNameWithPath));
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");

            // NOTE: Here I am just setting the result on the Task and not really doing any async stuff. 
            // But let's say you do stuff like contacting a File hosting service to get the file, then you would do 'async' stuff here.

            return Task.FromResult(response);
        }
    }
}