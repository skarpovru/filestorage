﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using FileStorage.Data.Models;

namespace FileStorage.Node.Services
{
    public class ManagerClientService
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly string _nodeKey = ConfigurationManager.AppSettings["NodeKey"];
        private readonly string _managerUrl = ConfigurationManager.AppSettings["ManagerUrl"];
        public TransactionInformation<FileLocationDetailResponse> GetFileLocationDetail(Guid id, bool toWrite)
        {
            TransactionInformation<FileLocationDetailResponse> result = new TransactionInformation<FileLocationDetailResponse>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_managerUrl + "/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync("FileOperations?id=" + id + "&login=kserks&nodeKey=" + _nodeKey + "&toWrite=" + toWrite).Result;
                //Logger.Info("Get started");
                if (response.IsSuccessStatusCode)
                {
                    result.Result = response.Content.ReadAsAsync<FileLocationDetailResponse>().Result;
                }
                else
                {
                    string errorMessage = response.Content.ReadAsStringAsync().Result.Trim('"');
                    result.StatusCode = response.StatusCode;
                    result.Success = false;
                    result.ErrorMessages.Add(errorMessage);
                    Logger.Error("Cannot get data from " + _managerUrl + ". Error: " + response.StatusCode + " " + errorMessage);
                }
            }
            return result;
        }

        public TransactionInformation SendUploadSuccessResult(Guid id, string fileHash = null)
        {
            TransactionInformation result = new TransactionInformation();


            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_managerUrl + "/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.

                HttpResponseMessage response = client.PostAsync("FileOperations?id=" + id + "&login=kserks&nodeKey=" + _nodeKey + (fileHash != null ? "&hash=" + fileHash : ""), null).Result;
                //Logger.Info("Get started");
                if (response.IsSuccessStatusCode)
                {
                    //result.Result = response.Content.ReadAsAsync<FileLocationDetailResponse>().Result;
                    //Logger.Info("{0}\t${1}\t{2}", result.Result.Path + "\\" + result.Result.ContainerName, result.Result.FileName, result.Result.FileExtension);
                }
                else
                {
                    string errorMessage = response.Content.ReadAsStringAsync().Result.Trim('"');
                    result.StatusCode = response.StatusCode;
                    result.Success = false;
                    result.ErrorMessages.Add(errorMessage);
                    Logger.Error("Cannot get data from " + _managerUrl + ". Error: " + response.StatusCode + " " + errorMessage);
                    //return BadRequest(response.ReasonPhrase + ", " + errorMessage);
                }
            }
            return result;
        }
    }
}