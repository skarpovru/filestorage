﻿using System.Web.Http;
using FileStorage.Data.Models;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Microsoft.OData.Edm;

namespace FileStorage.Manager
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            
            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: "odata4",
                model: GetEdmModel());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static IEdmModel GetEdmModel()
        {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //builder.Namespace = typeof(FileCollection).Namespace;
            builder.Namespace = "Core";
            builder.EntitySet<FileCollection>("FileCollections");
            builder.EntitySet<ClientApplication>("ClientApplications");
            builder.EntitySet<FileVersion>("FileVersions"); 
            var edmModel = builder.GetEdmModel();
            return edmModel;
        }
    }
}
