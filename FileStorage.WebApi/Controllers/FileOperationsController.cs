﻿using System;
using System.Collections.Generic;
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
using System.Linq;
//using System.Linq.Expressions;
using System.Net;
//using System.Threading.Tasks;
using System.Web.Http;
using FileStorage.Data.Context;
using FileStorage.Data.Models;
//using NLog;


namespace FileStorage.Manager.Controllers
{
    //Authorize file storage servers (nodes) only 
    [AllowAnonymous]
    public class FileOperationsController : ApiController
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly FileStorageContext _context = new FileStorageContext();
        public IHttpActionResult Get(Guid id, string login, string nodeKey, bool toWrite)
        {
            if (!_context.Servers.Any(t => t.Key == nodeKey))
            {
                Logger.Error("Server not authorised, nodeKey: " + nodeKey);
                return Content(HttpStatusCode.Unauthorized, "Server not authorised");
            }
            if (!_context.TemporaryAuthorizations.Any(t => t.FileVersionId == id && t.Login == login && t.ToWrite == toWrite))
            {
                Logger.Error("User " + login + " does not have " + (toWrite ? "write" : "read") + " access to requested file " + id);
                return Content(HttpStatusCode.Unauthorized, "User does not have access to requested file");
            }
            IQueryable<FileVersion> files = _context.FileVersions.Where(t => t.Id == id);
            
            if (toWrite)
            {
                files = files.Where(t => t.Status.Uploadable);
            }
            else
            {
                files = files.Where(t => t.Status.Downloadable);
            }           

            IEnumerable<FileLocationDetailResponse> fileLocationDetail =
                files.Select(t => new FileLocationDetailResponse()
                {
                    FileName = t.Name,
                    FileExtension = t.Extension,
                    ContainerName = t.Container.Name,
                    Path = t.Container.FilesLocation.Path,
                    Hash = t.Hash,
                    Size = t.Size
                }).ToList();

            if (!fileLocationDetail.Any())
            {
                return NotFound();
            }
            FileLocationDetailResponse result = fileLocationDetail.First();
            Logger.Info("{0}\t${1}\t{2}", result.Path + "\\" + result.ContainerName, result.FileName, result.FileExtension);
            return Ok(result);
        }

        // POST api/values
        public IHttpActionResult Post([FromUri]Guid id, [FromUri] string login, [FromUri] string nodeKey, [FromUri] string hash = null)
        {
            if (!_context.Servers.Any(t => t.Key == nodeKey))
            {
                Logger.Error("Server not authorised, nodeKey: " + nodeKey);
                return Content(HttpStatusCode.Unauthorized, "Server not authorised");
            }
            FileVersion file = _context.FileVersions.FirstOrDefault(t => t.Id == id && t.Status.Uploadable);
            if (file == null)
            {
                return NotFound();
            }
            file.Status = _context.FileStatuses.FirstOrDefault(t => t.Active);
            file.ExpiredIfNotLoadedAt = null;
            file.UploadedAt = DateTimeOffset.UtcNow;
            file.Valid = true;
            if (hash != null)
                file.Hash = hash;
            //Delete temporary authorization requests to write for uploaded file
            /*var temporaryAuthorizations = _context.TemporaryAuthorizations.Where(t => t.FileId == id && t.ToWrite);
            _context.TemporaryAuthorizations.RemoveRange(temporaryAuthorizations);*/
            _context.SaveChanges();
            return Ok();
        }
        
        /*
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }*/
    }
}
