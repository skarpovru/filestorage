﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.OData.Client;

namespace FileStorage.Manager.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var context = new DataServiceContext(new Uri("http://services.odata.org/v4/TripPinServiceRW/"));
            //var people = context.People.Execute();
            
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
