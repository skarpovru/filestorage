﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using FileStorage.Data.Context;
using FileStorage.Data.Models;
using FileStorage.Data.Services;
using Microsoft.OData.Core;

namespace FileStorage.Manager.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using FileStorage.Data.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<FileCollection>("FileCollections");
    builder.EntitySet<Client>("Clients"); 
    builder.EntitySet<File>("Files"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [ODataRoutePrefix("FileCollections")]
    public class FileCollectionsController : ODataController
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private FileStorageContext _db = new FileStorageContext();
        //private IApplicationService _applicationService; //= new ApplicationService(;
        
        public IApplicationService ApplicationService { get; private set; }
        public FileCollectionsController(IApplicationService applicationService)
        {
            ApplicationService = applicationService;
        }
        /*
        To implement
         * Get collections
         * Get collection by int key
         * Get versions in collection
         * Add new collection with first version upload
         * Add new collections with first version upload
         * Update collection
         * Delete collection (and all versions)
         * Get versions
         * Get version by Guid key
         * Download version
         * Download versions
         * Add version (Collection ID, Version details) with upload
         * Delete version
        */
        // GET: odata4/FileCollections
        [EnableQuery]
        public async Task<IHttpActionResult> Get(string clientKey)
        {
            TransactionInformation<ClientApplication> client = ApplicationService.AuthorizeClient(clientKey);
            if (!client.Success)
            {
                return Content(client.StatusCode, string.Join(";", client.ErrorMessages));
            }
            TransactionInformation<IQueryable<FileCollection>> result = ApplicationService.GetFileCollections(client.Result.Id);
            if (!result.Success)
            {
                return Content(result.StatusCode, string.Join(";", result.ErrorMessages));
            }
            return Ok(result.Result);
        }
        // GET: odata4/FileCollections(5)
        [EnableQuery]
        public async Task<IHttpActionResult> Get([FromODataUri] int key)
        {
            IQueryable<FileCollection> result = _db.FileCollections.Where(p => p.Id == key);
            return Ok(SingleResult.Create(result));
        }



        // PUT: odata4/FileCollections(5) Update files collection (Name, Description)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<FileCollection> patch, [FromODataUri] int clientApplicationId)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FileCollection fileCollection = await _db.FileCollections.FindAsync(key);
            if (fileCollection == null)
            {
                return NotFound();
            }

            patch.Put(fileCollection);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(fileCollection);
        }

        // POST: odata4/FileCollections
        public async Task<IHttpActionResult> Post(NewFileCollectionDTO fileCollection, [FromODataUri] string clientKey)
        {
            TransactionInformation<ClientApplication> client = ApplicationService.AuthorizeClient(clientKey);
            if (!client.Success)
            {
                return Content(client.StatusCode, string.Join(";", client.ErrorMessages));
            }
            
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Do authorization for clientApplicationId

            /*if (fileCollection.HeadFileVersion == null)
            {
                return BadRequest("");
            }*/

            TransactionInformation<FileUploadDetail> result = await ApplicationService.CreateFileCollection(fileCollection, client.Result.Id);
            if (!result.Success)
            {
                return Content(result.StatusCode, new ODataError {ErrorCode = result.StatusCode.ToString(), Message = string.Join(";", result.ErrorMessages)});
            }

            return Created(result.Result);
        }

        /*// PATCH: odata4/FileCollections(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<FileCollection> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            FileCollection fileCollection = await _db.FileCollections.FindAsync(key);
            if (fileCollection == null)
            {
                return NotFound();
            }

            patch.Patch(fileCollection);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Exists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(fileCollection);
        }*/

        // DELETE: odata4/FileCollections(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            FileCollection fileCollection = await _db.FileCollections.FindAsync(key);
            if (fileCollection == null)
            {
                return NotFound();
            }

            _db.FileCollections.Remove(fileCollection);
            await _db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata4/FileCollections(5)/Client
        [EnableQuery]
        public SingleResult<ClientApplication> GetClientApplication([FromODataUri] int key)
        {
            return SingleResult.Create(_db.FileCollections.Where(m => m.Id == key).Select(m => m.ClientApplication));
        }

        // GET: odata4/FileCollections(5)/Files
        [EnableQuery]
        public IQueryable<FileVersion> GetFileVersions([FromODataUri] int key)
        {
            return _db.FileCollections.Where(m => m.Id == key).SelectMany(m => m.FileVersions);
        }

        private bool Exists(int key)
        {
            return _db.FileCollections.Any(p => p.Id == key);
        }
    }
}
