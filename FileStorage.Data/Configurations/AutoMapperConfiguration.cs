﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FileStorage.Data.Models;

namespace FileStorage.Data.Configurations
{
    public class EntityMappingProfile : Profile
    {
        protected override void Configure()
        {
            //Put CreateMap... Etc.. here
            Mapper.CreateMap<FileCollection, FileCollection>()
                .ForMember(t => t.ClientApplicationId, t => t.Ignore())
                .ForMember("HeadFileVersionId", t => t.Ignore());

            Mapper.CreateMap<FileVersion, FileVersion>()
                .ForMember(t => t.ContainerId, t => t.Ignore())
                .ForMember(t => t.ExpiredIfNotLoadedAt, t => t.Ignore())
                .ForMember(t => t.FileCollectionId, t => t.Ignore())
                .ForMember(t => t.HeadFileVersionInFileCollection, t => t.Ignore())
                .ForMember(t => t.LastAccessAt, t => t.Ignore())
                .ForMember(t => t.StatusId, t => t.Ignore())
                .ForMember(t => t.UploadedAt, t => t.Ignore())
                //.ForMember(t => t.UploadedBy, t => t.Ignore())
                .ForMember(t => t.Valid, t => t.Ignore())
                .ForMember(t => t.Version, t => t.Ignore());
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }
}
