﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.OData.Builder;

namespace FileStorage.Data.Models
{
    /// <summary>
    /// File collection
    /// </summary>
    public class FileCollection : IdEntity
    {
        /// <summary>
        /// File collection name, usually generic name for describing all files in collection (e.g. Yearly review results for Compliance)
        /// </summary>
        [Description("File collection name, usually generic name for describing all files in collection (e.g. Yearly review results for Compliance)")]
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "Name length should be between 1 and 200")]
        public string Name { get; set; }

        /// <summary>
        /// Generic description for collection 
        /// </summary>
        [Description("Generic description for collection")]
        public string Description { get; set; }

        /// <summary>
        /// Is multiple file versions allowed (if not then onle single file could be added to the collection)
        /// </summary>
        [Description("Is multiple file versions allowed (if not then onle single file could be added to the collection)")]
        [Display(Name = "Track file changes")]
        public bool SupportVersioning { get; set; }

        /// <summary>
        /// Is file archiving allowed
        /// </summary>
        [Description("Is file archiving allowed")]
        [Display(Name = "Archive files")]
        public bool Archivable { get; set; }

        /// <summary>
        /// File could be deleted after defined number of days after the most recent access request
        /// </summary>
        [Description("File could be deleted after defined number of days after the most recent access request")]
        [Display(Name = "Expire in days")]
        public int ExpireInDays { get; set; }

        /// <summary>
        /// Client application to whom collection belongs to (collection owner)
        /// </summary>
        [Description("Client application to whom collection belongs to (collection owner)")]
        [IgnoreDataMember]
        [HttpBindNever]
        public int ClientApplicationId { get; set; }
        [ForeignKey("ClientApplicationId")]
        public virtual ClientApplication ClientApplication { get; set; }

        /// <summary>
        /// Head file version in collection (Foreign key defined in Fluent API)
        /// </summary>
        [Description("Head file version in collection")]
        public virtual FileVersion HeadFileVersion { get; set; }

        /// <summary>
        /// File versions in collection
        /// </summary>
        [Description("File versions in collection")]
        [InverseProperty("FileCollection")]
        [Contained]
        public virtual ICollection<FileVersion> FileVersions { get; set; }
    }

    /// <summary>
    /// File instance
    /// </summary>
    public class FileVersion
    {
        /// <summary>
        /// File guid, unique for each file (e.g. 0f8fad5b-d9cb-469f-a165-70867728950e)
        /// </summary>
        [Description("File guid, unique for each file (e.g. 0f8fad5b-d9cb-469f-a165-70867728950e)")]
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// File name without extension
        /// </summary>
        [Description("File name without extension")]
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "Name length should be between 1 and 200")]
        public string Name { get; set; }

        /// <summary>
        /// File extension (e.g., docx, xlsx; zip)
        /// </summary>
        [Description("File extension (e.g. docx, xlsx; zip)")]
        [Required, StringLength(10, MinimumLength = 1, ErrorMessage = "Extension length should be between 1 and 10")]
        public string Extension { get; set; }

        /// <summary>
        /// File version number, set internaly based on number of files in collection
        /// </summary>
        [HttpBindNever]
        [Description("File version number, set internaly based on number of files in collection")]
        public int Version { get; set; }

        /// <summary>
        /// File version description (i.e. change log)
        /// </summary>
        [Description("File version description (i.e. change log)")]
        [Display(Name = "Change log")]
        public string VersionDescription { get; set; }

        /// <summary>
        /// Hash for file integrity verification (e.g. f411ca094275f727bc381363e9f4cf8b)
        /// </summary>
        [Description("Hash for file integrity verification (e.g. f411ca094275f727bc381363e9f4cf8b)")]
        [Display(Name = "Md5 checksum")]
        public string Hash { get; set; }

        /// <summary>
        /// File size, defined on a client side before upload and verified on server side
        /// </summary>
        [Description("File size, defined on a client side before upload and verified on server side")]
        [Required, Display(Name="Size in bytes")]
        public long Size { get; set; }

        /// <summary>
        /// Date and time when the record will be deleted if file not uploaded, set internaly based on server rules
        /// </summary>
        [Description("Date and time when the record will be deleted if file not uploaded, set internaly based on server rules")]
        [Display(Name = "Expired if file not uploaded")]
        //[HttpBindNever]
        [IgnoreDataMember]
        public DateTimeOffset? ExpiredIfNotLoadedAt { get; set; }

        /// <summary>
        /// Result of file integrity verification based on stored hash
        /// </summary>
        [Description("Result of file integrity verification based on stored hash")]
        [HttpBindNever]
        public bool Valid { get; set; }

        /// <summary>
        /// Container where the file is stored, set internally
        /// </summary>
        [Description("Container where the file is stored, set internally")]
        [HttpBindNever]
        public int ContainerId { get; set; }
        [ForeignKey("ContainerId")]
        public virtual Container Container { get; set; }

        /// <summary>
        /// Collection that owns the file, set internally
        /// </summary>
        [Description("Collection that owns the file, set internally")]
        [HttpBindNever]
        public int FileCollectionId { get; set; }
        [ForeignKey("FileCollectionId")]
        //[InverseProperty("Files")]
        public virtual FileCollection FileCollection { get; set; }
        
        
        /// <summary>
        /// Supplemental property for mapping of the file as the head version in the collection
        /// </summary>
        [Description("Supplemental property for mapping of the file as the head version in the collection")]
        //public int? HeadVersionInFileCollectionId { get; set; }
        //[InverseProperty("HeadFileVersion")]
        //[ForeignKey("HeadVersionInFileCollectionId")]
        [HttpBindNever]
        public virtual FileCollection HeadFileVersionInFileCollection { get; set; }

        /// <summary>
        /// ID of employee who last updated the record, set internally (e.g. 7357969)
        /// </summary>
        [Description("ID of employee who last updated the record, set internally (e.g. 7357969)")]
        [Display(Name = "Employee ID"), StringLength(11, MinimumLength = 5, ErrorMessage = "Employee ID length should be between 5 and 11")]
        [HttpBindNever]
        public string UploadedBy { get; set; }
        [HttpBindNever]
        public DateTimeOffset? UploadedAt { get; set; }
        [HttpBindNever]
        public DateTimeOffset? LastAccessAt { get; set; }

        /// <summary>
        /// File status, set internally (i.e. Active, Disposed, Not Loaded)
        /// </summary>
        [Description("File status, set internally (i.e. Active, Disposed, Not Loaded)")]
        [HttpBindNever]
        public int StatusId { get; set; }
        [HttpBindNever]
        [ForeignKey("StatusId")]
        public virtual FileStatus Status { get; set; }

        /// <summary>
        /// Access requests from users for operations with files (upload/download)
        /// </summary>
        [Description("Requests from users for operations with files (upload/download)")]
        [Contained]
        public virtual ICollection<Request> Requests { get; set; }
    }

    /// <summary>
    /// Access request from user for operation with file (upload or download)
    /// </summary>
    public class Request : IdEntity
    {
        /// <summary>
        /// File instance
        /// </summary>
        [Description("File instance")]
        public Guid FileVersionId { get; set; }
        [ForeignKey("FileVersionId")]
        public virtual FileVersion FileVersion { get; set; }

        /// <summary>
        /// Type of operation with file (i.e. upload/download)
        /// </summary>
        [Description("Type of operation with file (i.e. upload/download)")]
        [Display(Name = "Is permission to write (default read)")]
        public bool ToWrite { get; set; }

        /// <summary>
        /// Date and time of file access request
        /// </summary>
        [Description("Date and time of file access request")]
        public DateTimeOffset RequestedAt { get; set; }

        /// <summary>
        /// ID of employee who requested access to the file
        /// </summary>
        [Description("ID of employee who requested access to the file")]
        [Index]
        [Display(Name = "Employee ID"), StringLength(11, MinimumLength = 5, ErrorMessage = "Employee ID length should be between 5 and 11")]
        public string RequestedBy { get; set; }
    }

    /// <summary>
    /// Temporary access authorization for employee to the specific file
    /// </summary>
    public class TemporaryAuthorization : IdEntity
    {
        /// <summary>
        /// Domain login of employee who requested access to the file
        /// </summary>
        [Description("Domain login of employee who requested access to the file")]
        /*[Display(Name = "Employee ID"), StringLength(11, MinimumLength = 5, ErrorMessage = "Employee ID length should be between 5 and 11")]
        public string EmployeeId { get; set; }*/
        [Index]
        [Display(Name = "Employee Login"), StringLength(50, MinimumLength = 5, ErrorMessage = "Employee Login length should be between 5 and 50")]
        public string Login { get; set; }

        /// <summary>
        /// Type of operation with file (i.e. upload/download)
        /// </summary>
        [Description("Type of operation with file (i.e. upload/download)")]
        [Display(Name = "Is permission to write (default read)")]
        public bool ToWrite { get; set; }

        /// <summary>
        /// Temporary access authorization will expire at
        /// </summary>
        [Description("Temporary access authorization will expire at")]
        public DateTimeOffset ExpireAt { get; set; }

        /// <summary>
        /// File instance
        /// </summary>
        [Description("File instance")]
        public Guid FileVersionId { get; set; }
        [ForeignKey("FileVersionId")]
        public virtual FileVersion FileVersion { get; set; }

        /*public int ServerId { get; set; }
        [ForeignKey("ServerId")]
        public virtual Server Server { get; set; }*/
    }

    /// <summary>
    /// Details with file location storage for file storage node 
    /// </summary>
    public class FileLocationDetailResponse
    {
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ContainerName { get; set; }
        public string Path { get; set; }
        public string Hash { get; set; }
        public long Size { get; set; }
    }

    /// <summary>
    /// File upload result from node to manager
    /// </summary>
    public class FileUploadResult
    {
        //Empty constructor needed only for Serialization
        public FileUploadResult() { }
        public FileUploadResult(string name)
        {
            Name = name;
            Success = true;
        }
        public string Name { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }

    /*public class FileLocationDetailRequestDTO
    {
        public Guid FileId { get; set; }
        public string Login { get; set; }

    }*/

    /*enum FileOperation
    {
        Read,
        Write, 
        Rewrite,
        Delete
    };*/

    /// <summary>
    /// File status to show current stage of life cycle (i.e. Active, Disposed, Not Loaded)
    /// </summary>
    public class FileStatus : IdEntity
    {
        [Required, StringLength(50, MinimumLength = 4, ErrorMessage = "Name length should be between 4 and 50")]
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool NotLoaded { get; set; }
        public bool Archived { get; set; }
        public bool Disposed { get; set; }
        public bool Missing { get; set; }
        public bool Modified { get; set; }
        public bool Downloadable { get; set; }
        public bool Uploadable { get; set; }
        [IgnoreDataMember]
        public virtual ICollection<FileVersion> FileVersions { get; set; }
    }
}
