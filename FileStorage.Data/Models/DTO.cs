﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.Data.Models
{
    public class NewFileCollectionDTO : IdEntity
    {
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "Name length should be between 1 and 200")]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? SupportVersioning { get; set; }
        public bool? Archivable { get; set; }
        public int? ExpireInDays { get; set; }
        //public int ClientApplicationId { get; set; }
        public int? PreferableServerId { get; set; }
        [Required]
        public NewHeadFileVersionDTO HeadFileVersion { get; set; }
        [Required]
        public PersonDTO Employee { get; set; }
    }

    public class NewHeadFileVersionDTO
    {
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "Name length should be between 1 and 200")]
        public string Name { get; set; }
        [Required, StringLength(10, MinimumLength = 1, ErrorMessage = "Extension length should be between 1 and 10")]
        public string Extension { get; set; }
        public string VersionDescription { get; set; }
        public string Hash { get; set; }
        public long Size { get; set; }
    }

    public class PersonDTO
    {
        [Required, StringLength(11, MinimumLength = 5, ErrorMessage = "Employee ID length should be between 5 and 11")]
        public string EmployeeId { get; set; }
        [Required, StringLength(50, MinimumLength = 5, ErrorMessage = "Employee Login length should be between 5 and 50")]
        public string Login { get; set; }
    }

    /// <summary>
    /// Details for file upload to the node
    /// </summary>
    public class FileUploadDetail
    {
        public Guid FileId { get; set; }
        public string Url { get; set; }

    }
}
