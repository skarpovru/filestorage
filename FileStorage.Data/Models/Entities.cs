﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace FileStorage.Data.Models
{

    public class TransactionInformation
    {
        public TransactionInformation()
        {
            StatusCode = HttpStatusCode.InternalServerError;
            Success = true;
            ErrorMessages = new List<string>();
        }
        public HttpStatusCode StatusCode { get; set; }
        public bool Success { get; set; }
        public List<string> ErrorMessages { get; set; }
    }
    public class TransactionInformation<T> : TransactionInformation
    {
        public T Result { get; set; }
    }
    //Void result
    /*public class OperationResult : TransactionInformation
    {
    }*/


    public interface IIdEntity
    {
        int Id { get; set; }
    }

    public interface IBaseEntity : IIdEntity
    {
        DateTime CreatedAt { get; set; }
        int? CreatedById { get; set; }
        DateTime UpdatedAt { get; set; }
        int? UpdatedById { get; set; }
        int RecordVersion { get; }
    }

    public abstract class IdEntity : IIdEntity
    {
        [Key]
        public int Id { get; set; }
    }

    public abstract class BaseEntity : IdEntity, IBaseEntity
    {
        public BaseEntity()
        {
            DateTime now = DateTime.Now;
            CreatedAt = now;
            UpdatedAt = now;
        }
        public DateTime CreatedAt { get; set; }
        public int? CreatedById { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int? UpdatedById { get; set; }

        [ConcurrencyCheck]
        public int RecordVersion { get; internal set; }
    }
}
