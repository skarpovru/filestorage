﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Builder;

namespace FileStorage.Data.Models
{
    /// <summary>
    /// Instance of file storage node (usually one per each application server)
    /// </summary>
    public class Server : IdEntity
    {
        /// <summary>
        /// Node name, may be a physical server name
        /// </summary>
        [Description("Node name, may be a physical server name")]
        [Required, StringLength(100, MinimumLength = 4, ErrorMessage = "Name length should be between 4 and 100")]
        public string Name { get; set; }

        /// <summary>
        /// Url of the node for files upload/download by users
        /// </summary>
        [Description("Url of the node for files upload/download by users")]
        [Required, StringLength(200, MinimumLength = 5, ErrorMessage = "Url length should be between 5 and 200")]
        public string ClientUrl { get; set; }

        /// <summary>
        /// Server key for node authentication (e.g. 9Hr3VsQ4)
        /// </summary>
        [Description("Server key for node authentication (e.g. 9Hr3VsQ4)")]
        public string Key { get; set; }

        /// <summary>
        /// Code of country where server located (e.g. GBR, DEU)
        /// </summary>
        [Description("Code of country where server located (e.g. GBR, DEU)")]
        [Required]
        public string CountryCode { get; set; }

        /// <summary>
        /// Server online status
        /// </summary>
        [Description("Server online status")]
        [Display(Name = "Server online")]
        public bool Available { get; set; }

        /// <summary>
        /// Available for adding new files
        /// </summary>
        [Description("Available for adding new files")]
        [Display(Name = "Open for write")]
        public bool OpenForWrite { get; set; }

        /// <summary>
        /// Client applications which use the server as preferable
        /// </summary>
        [Description("Client applications which use the server as preferable")]
        [Contained]
        public virtual ICollection<ClientApplication> ClientApplications { get; set; }

        /// <summary>
        /// Physical locations for files on the server 
        /// </summary>
        [Description("Physical locations for files on the server")]
        [Contained]
        public virtual ICollection<FilesLocation> FilesLocations { get; set; }
    }

    /// <summary>
    /// Physical folder on the server for files placement
    /// </summary>
    public class FilesLocation : IdEntity
    {
        /// <summary>
        /// File location name (e.g. Primary, Secondary)
        /// </summary>
        [Description("File location name (e.g. Primary, Secondary)")]
        [Required, StringLength(100, MinimumLength = 4, ErrorMessage = "Name length should be between 4 and 100")]
        public string Name { get; set; }

        /// <summary>
        /// Path to the physical folder on the server
        /// </summary>
        [Description("Path to the physical folder on the server")]
        [Required, StringLength(40, MinimumLength = 2, ErrorMessage = "Name length should be between 2 and 40")]
        public string Path { get; set; }

        /// <summary>
        /// Capacity of the file location (usually depends on capacity of a hard drive dedicated for file storage)
        /// </summary>
        [Description("Capacity of the file location (usually depends on capacity of a hard drive dedicated for file storage)")]
        [Display(Name = "Capacity in MB")]
        public int Capacity { get; set; }

        /// <summary>
        /// Total used space by all files in the location
        /// </summary>
        [Description("Total used space by all files in the location")]
        [Display(Name = "Consumed in MB")]
        public int Consumed { get; set; }

        /// <summary>
        /// Available for adding new files
        /// </summary>
        [Description("Available for adding new files")]
        [Display(Name = "Open for write")]
        public bool OpenForWrite { get; set; }

        /// <summary>
        /// Location online status
        /// </summary>
        [Description("Location online status")]
        [Display(Name = "Storage accessible")]
        public bool Available { get; set; }

        /// <summary>
        /// Instance of the storage node
        /// </summary>
        [Description("Instance of the storage node")]
        public int ServerId { get; set; }
        [ForeignKey("ServerId")]
        public virtual Server Server { get; set; }

        /// <summary>
        /// Containers which placed in the files location
        /// </summary>
        [Description("Containers which placed in the files location")]
        [Contained]
        public virtual ICollection<Container> Containers { get; set; }
    }

    /// <summary>
    /// Container for balancing file storage inside files location 
    /// </summary>
    public class Container : IdEntity
    {
        /// <summary>
        /// Container name to be used as folder name, unique inside files location, preferable equal Id (e.g. 1, 2)
        /// </summary>
        [Description("Container name to be used as folder name, unique inside files location, preferable equal Id (e.g. 1, 2)")]
        public string Name { get; set; }

        /// <summary>
        /// Available for adding new files
        /// </summary>
        [Description("Available for adding new files")]
        [Display(Name = "Open for write")]
        public bool OpenForWrite { get; set; }

        /// <summary>
        /// Container online status
        /// </summary>
        [Description("Container online status")]
        [Display(Name = "Container accessible")]
        public bool Available { get; set; }

        /// <summary>
        /// Physical folder on the server for files placement
        /// </summary>
        [Description("Physical folder on the server for files placement")]
        public int FilesLocationId { get; set; }
        [ForeignKey("FilesLocationId")]
        public virtual FilesLocation FilesLocation { get; set; }

        /// <summary>
        /// Client application to whom container belongs to
        /// </summary>
        [Description("Client application to whom container belongs to")]
        public int ClientApplicationId { get; set; }
        [ForeignKey("ClientApplicationId")]
        public virtual ClientApplication ClientApplication { get; set; }

        /// <summary>
        /// File versions in container
        /// </summary>
        [Description("File versions in container")]
        [Contained]
        public virtual ICollection<FileVersion> FileVersions { get; set; }
    }

    /*public class ContainerStatus : IdEntity
    {
        [Required, StringLength(50, MinimumLength = 4, ErrorMessage = "Name length should be between 4 and 50")]
        public string Name { get; set; }
        public bool IsArchived { get; set; }
        public bool IsDisposed { get; set; }
        public virtual ICollection<Container> Containers { get; set; }
    }*/

    /// <summary>
    /// Client application who can use file storage
    /// </summary>
    public class ClientApplication : IdEntity
    {
        /// <summary>
        /// Client application name
        /// </summary>
        [Description("Client application name")]
        public string Name { get; set; }

        /// <summary>
        /// Application key for client authentication
        /// </summary>
        [Description("Application key for client authentication")]
        public string Key { get; set; }

        /// <summary>
        /// Preferable server to store files (some applications could require to store files only in specific country)
        /// </summary>
        [Description("Preferable server to store files (some applications could require to store files only in specific country)")]
        public int PreferableServerId { get; set; }
        [ForeignKey("PreferableServerId")]
        public virtual Server PreferableServer { get; set; }

        /// <summary>
        /// Containers with files assigned to the client
        /// </summary>
        [Description("Containers with files assigned to the client")]
        [IgnoreDataMember]
        public virtual ICollection<Container> Containers { get; set; }
    }

    
}
