namespace FileStorage.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class New : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Container", "StatusId", "dbo.ContainerStatus");
            DropIndex("dbo.Container", new[] { "StatusId" });
            AddColumn("dbo.Container", "Available", c => c.Boolean(nullable: false));
            AddColumn("dbo.FilesLocation", "Available", c => c.Boolean(nullable: false));
            AddColumn("dbo.Server", "ClientUrl", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.Server", "CountryCode", c => c.String(nullable: false));
            AddColumn("dbo.Server", "OpenForWrite", c => c.Boolean(nullable: false));
            AlterColumn("dbo.FileVersion", "UploadedBy", c => c.String());
            DropColumn("dbo.Container", "Valid");
            DropColumn("dbo.Container", "StatusId");
            DropColumn("dbo.FilesLocation", "Valid");
            DropColumn("dbo.Server", "Url");
            DropTable("dbo.ContainerStatus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ContainerStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsArchived = c.Boolean(nullable: false),
                        IsDisposed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Server", "Url", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.FilesLocation", "Valid", c => c.Boolean(nullable: false));
            AddColumn("dbo.Container", "StatusId", c => c.Int(nullable: false));
            AddColumn("dbo.Container", "Valid", c => c.Boolean(nullable: false));
            AlterColumn("dbo.FileVersion", "UploadedBy", c => c.String(maxLength: 11));
            DropColumn("dbo.Server", "OpenForWrite");
            DropColumn("dbo.Server", "CountryCode");
            DropColumn("dbo.Server", "ClientUrl");
            DropColumn("dbo.FilesLocation", "Available");
            DropColumn("dbo.Container", "Available");
            CreateIndex("dbo.Container", "StatusId");
            AddForeignKey("dbo.Container", "StatusId", "dbo.ContainerStatus", "Id");
        }
    }
}
