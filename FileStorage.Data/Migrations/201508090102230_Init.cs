namespace FileStorage.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientApplication",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Key = c.String(),
                        PreferableServerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Server", t => t.PreferableServerId)
                .Index(t => t.PreferableServerId);
            
            CreateTable(
                "dbo.Container",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OpenForWrite = c.Boolean(nullable: false),
                        Valid = c.Boolean(nullable: false),
                        FilesLocationId = c.Int(nullable: false),
                        ClientApplicationId = c.Int(nullable: false),
                        StatusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClientApplication", t => t.ClientApplicationId)
                .ForeignKey("dbo.FilesLocation", t => t.FilesLocationId)
                .ForeignKey("dbo.ContainerStatus", t => t.StatusId)
                .Index(t => t.FilesLocationId)
                .Index(t => t.ClientApplicationId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.FilesLocation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Path = c.String(nullable: false, maxLength: 40),
                        Capacity = c.Int(nullable: false),
                        Consumed = c.Int(nullable: false),
                        OpenForWrite = c.Boolean(nullable: false),
                        Valid = c.Boolean(nullable: false),
                        ServerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Server", t => t.ServerId)
                .Index(t => t.ServerId);
            
            CreateTable(
                "dbo.Server",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Url = c.String(nullable: false, maxLength: 200),
                        Key = c.String(),
                        Available = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FileVersion",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Extension = c.String(nullable: false, maxLength: 10),
                        Version = c.Int(nullable: false),
                        VersionDescription = c.String(),
                        Hash = c.String(),
                        Size = c.Long(nullable: false),
                        ExpiredIfNotLoadedAt = c.DateTimeOffset(precision: 7),
                        Valid = c.Boolean(nullable: false),
                        ContainerId = c.Int(nullable: false),
                        FileCollectionId = c.Int(nullable: false),
                        UploadedBy = c.String(maxLength: 11),
                        UploadedAt = c.DateTimeOffset(precision: 7),
                        LastAccessAt = c.DateTimeOffset(precision: 7),
                        StatusId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Container", t => t.ContainerId)
                .ForeignKey("dbo.FileCollection", t => t.FileCollectionId)
                .ForeignKey("dbo.FileStatus", t => t.StatusId)
                .Index(t => t.ContainerId)
                .Index(t => t.FileCollectionId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.FileCollection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(),
                        SupportVersioning = c.Boolean(nullable: false),
                        Archivable = c.Boolean(nullable: false),
                        ExpireInDays = c.Int(nullable: false),
                        ClientApplicationId = c.Int(nullable: false),
                        HeadFileVersionId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClientApplication", t => t.ClientApplicationId)
                .ForeignKey("dbo.FileVersion", t => t.HeadFileVersionId)
                .Index(t => t.ClientApplicationId)
                .Index(t => t.HeadFileVersionId);
            
            CreateTable(
                "dbo.Request",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileVersionId = c.Guid(nullable: false),
                        ToWrite = c.Boolean(nullable: false),
                        RequestedAt = c.DateTimeOffset(nullable: false, precision: 7),
                        RequestedBy = c.String(maxLength: 11),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FileVersion", t => t.FileVersionId)
                .Index(t => t.FileVersionId)
                .Index(t => t.RequestedBy);
            
            CreateTable(
                "dbo.FileStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        NotLoaded = c.Boolean(nullable: false),
                        Archived = c.Boolean(nullable: false),
                        Disposed = c.Boolean(nullable: false),
                        Missing = c.Boolean(nullable: false),
                        Modified = c.Boolean(nullable: false),
                        Downloadable = c.Boolean(nullable: false),
                        Uploadable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContainerStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsArchived = c.Boolean(nullable: false),
                        IsDisposed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TemporaryAuthorization",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(maxLength: 50),
                        ToWrite = c.Boolean(nullable: false),
                        ExpireAt = c.DateTimeOffset(nullable: false, precision: 7),
                        FileVersionId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FileVersion", t => t.FileVersionId)
                .Index(t => t.Login)
                .Index(t => t.FileVersionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TemporaryAuthorization", "FileVersionId", "dbo.FileVersion");
            DropForeignKey("dbo.Container", "StatusId", "dbo.ContainerStatus");
            DropForeignKey("dbo.FileVersion", "StatusId", "dbo.FileStatus");
            DropForeignKey("dbo.Request", "FileVersionId", "dbo.FileVersion");
            DropForeignKey("dbo.FileCollection", "HeadFileVersionId", "dbo.FileVersion");
            DropForeignKey("dbo.FileVersion", "FileCollectionId", "dbo.FileCollection");
            DropForeignKey("dbo.FileCollection", "ClientApplicationId", "dbo.ClientApplication");
            DropForeignKey("dbo.FileVersion", "ContainerId", "dbo.Container");
            DropForeignKey("dbo.FilesLocation", "ServerId", "dbo.Server");
            DropForeignKey("dbo.ClientApplication", "PreferableServerId", "dbo.Server");
            DropForeignKey("dbo.Container", "FilesLocationId", "dbo.FilesLocation");
            DropForeignKey("dbo.Container", "ClientApplicationId", "dbo.ClientApplication");
            DropIndex("dbo.TemporaryAuthorization", new[] { "FileVersionId" });
            DropIndex("dbo.TemporaryAuthorization", new[] { "Login" });
            DropIndex("dbo.Request", new[] { "RequestedBy" });
            DropIndex("dbo.Request", new[] { "FileVersionId" });
            DropIndex("dbo.FileCollection", new[] { "HeadFileVersionId" });
            DropIndex("dbo.FileCollection", new[] { "ClientApplicationId" });
            DropIndex("dbo.FileVersion", new[] { "StatusId" });
            DropIndex("dbo.FileVersion", new[] { "FileCollectionId" });
            DropIndex("dbo.FileVersion", new[] { "ContainerId" });
            DropIndex("dbo.FilesLocation", new[] { "ServerId" });
            DropIndex("dbo.Container", new[] { "StatusId" });
            DropIndex("dbo.Container", new[] { "ClientApplicationId" });
            DropIndex("dbo.Container", new[] { "FilesLocationId" });
            DropIndex("dbo.ClientApplication", new[] { "PreferableServerId" });
            DropTable("dbo.TemporaryAuthorization");
            DropTable("dbo.ContainerStatus");
            DropTable("dbo.FileStatus");
            DropTable("dbo.Request");
            DropTable("dbo.FileCollection");
            DropTable("dbo.FileVersion");
            DropTable("dbo.Server");
            DropTable("dbo.FilesLocation");
            DropTable("dbo.Container");
            DropTable("dbo.ClientApplication");
        }
    }
}
