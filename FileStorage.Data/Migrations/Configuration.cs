using FileStorage.Data.Models;

namespace FileStorage.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Context.FileStorageContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Context.FileStorageContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Servers.AddOrUpdate(
              p => p.Id,
              new Server { Id = 1, Name = "Primary", ClientUrl = "http://localhost:22289/api/Files", Key = "9Hr3VsQ4", Available = true, OpenForWrite = true, CountryCode = "DEU"},
              new Server { Id = 2, Name = "Secondary", ClientUrl = "http://localhost:12346/api/Files", Key = "Key2", Available = true, OpenForWrite = true, CountryCode = "GBR" }
            );

            context.FilesLocations.AddOrUpdate(
              p => p.Id,
              new FilesLocation { Id = 1, Name = "Primary", Capacity = 200, OpenForWrite = true, Path = "D:\\FileStorage", ServerId = 1 },
              new FilesLocation { Id = 2, Name = "Secondary", Capacity = 50, OpenForWrite = true, Path = "C:\\FileStorage", ServerId = 1 }
            );

            context.SaveChanges();

            context.ClientApplications.AddOrUpdate(
              p => p.Id,
              new ClientApplication { Id = 1, Name = "Application1", Key = "Key1", PreferableServerId = 1 },
              new ClientApplication { Id = 2, Name = "Application2", Key = "Key2", PreferableServerId = 2 }
            );

            /*context.ContainerStatuses.AddOrUpdate(
              p => p.Id,
              new ContainerStatus { Id = 1, Name = "Active" },
              new ContainerStatus { Id = 2, Name = "Archived", IsArchived = true },
              new ContainerStatus { Id = 3, Name = "Disposed", IsDisposed = true }
            );*/

            context.SaveChanges();

            context.Containers.AddOrUpdate(
              p => p.Id,
              new Container { Id = 1, Name = "1", OpenForWrite = true, Available = true, FilesLocationId = 1, ClientApplicationId = 1 }
            );

             context.FileCollections.AddOrUpdate(
              p => p.Id,
              new FileCollection { Id = 1, Name = "Test File", ClientApplicationId = 1},
              new FileCollection { Id = 2, Name = "Test File2", ClientApplicationId = 1, SupportVersioning = true }
            );

            context.FileStatuses.AddOrUpdate(
              p => p.Id,
              new FileStatus { Id = 1, Name = "Active", Downloadable = true, Uploadable = false, Active = true},
              new FileStatus { Id = 2, Name = "Not Loaded", Downloadable = false, Uploadable = true, NotLoaded = true },
              new FileStatus { Id = 3, Name = "Archived", Downloadable = true, Uploadable = false, Archived = true },
              new FileStatus { Id = 4, Name = "Disposed", Downloadable = false, Uploadable = false, Disposed = true },
              new FileStatus { Id = 5, Name = "Missing", Downloadable = false, Uploadable = true, Missing = true },
              new FileStatus { Id = 6, Name = "Modified", Downloadable = true, Uploadable = false, Modified = true }
            );
            context.SaveChanges();

            context.FileVersions.AddOrUpdate(
              p => p.Id,
              new FileVersion { Id = Guid.Parse("0f8fad5b-d9cb-469f-a165-70867728950e"), Name = "TestFile", Extension = "txt", Size = 13, Hash = "f411ca094275f727bc381363e9f4cf8b", FileCollectionId = 1, StatusId = 1, ContainerId = 1, Valid = true },
              new FileVersion { Id = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7"), Name = "Test123", Extension = "jpg", Size = 44656, Hash = "e270deb9d961326ffe4034f68c07dfab", FileCollectionId = 2, StatusId = 2, ContainerId = 1, Valid = false, ExpiredIfNotLoadedAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new FileVersion { Id = Guid.Parse("858992ac-040f-4157-9e20-eee1d9575086"), Name = "calibre-64bit-2.11.0", Extension = "msi", Size = 68542464, Hash = null, FileCollectionId = 2, StatusId = 2, ContainerId = 1, Valid = false, ExpiredIfNotLoadedAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new FileVersion { Id = Guid.Parse("e1a62075-698a-406f-887e-7465bd632b00"), Name = "Adobe Media Encoder x64 v7.1", Extension = "zip", Size = 436634565, Hash = null, FileCollectionId = 2, StatusId = 2, ContainerId = 1, Valid = false, ExpiredIfNotLoadedAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new FileVersion { Id = Guid.Parse("69409776-e1fb-45f0-b3e9-039dd3c0eccc"), Name = "SetupVoipConnect-nonoh", Extension = "exe", Size = 44656, Hash = null, FileCollectionId = 2, StatusId = 2, ContainerId = 1, Valid = false, ExpiredIfNotLoadedAt = DateTimeOffset.UtcNow.AddMinutes(30) }
            );
            context.SaveChanges();
            context.TemporaryAuthorizations.AddOrUpdate(
              p => p.FileVersionId,
              new TemporaryAuthorization { FileVersionId = Guid.Parse("0f8fad5b-d9cb-469f-a165-70867728950e"), ToWrite = false, Login = "kserks", ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30)},
              new TemporaryAuthorization { FileVersionId = Guid.Parse("7c9e6679-7425-40de-944b-e07fc1f90ae7"), ToWrite = true, Login = "kserks", ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new TemporaryAuthorization { FileVersionId = Guid.Parse("858992ac-040f-4157-9e20-eee1d9575086"), ToWrite = true, Login = "kserks", ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new TemporaryAuthorization { FileVersionId = Guid.Parse("e1a62075-698a-406f-887e-7465bd632b00"), ToWrite = true, Login = "kserks", ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30) },
              new TemporaryAuthorization { FileVersionId = Guid.Parse("69409776-e1fb-45f0-b3e9-039dd3c0eccc"), ToWrite = true, Login = "kserks", ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30) }
            );
            //add-migration -projectname filestorage.data -startupproject filestorage.manager MigrationName
            //update-database -projectname filestorage.data -startupproject filestorage.manager
            //update-database -projectname filestorage.data -startupproject filestorage.manager –TargetMigration: $InitialDatabase
            //update-database -projectname filestorage.data -startupproject filestorage.manager -Force
        }
    }
}
