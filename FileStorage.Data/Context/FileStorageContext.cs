using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using FileStorage.Data.Models;

namespace FileStorage.Data.Context
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class FileStorageContext : DbContext
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        // Your context has been configured to use a 'FileStorage' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'FileStorage.Data.DataServices.FileStorage' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'FileStorage' 
        // connection string in the application configuration file.
        public FileStorageContext()
            : base("name=FileStorageContext")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Database.Log = t => Logger.Info("SQL: " + t);
        }

        public int ExecuteSql(string sql)
        {
            return base.Database.ExecuteSqlCommand(sql);
        }

        public IEnumerable<int> TruncateTable<TElement>(string sql)
            where TElement : class, IIdEntity
        {
            IEnumerable<string> linkedTables = this.GetTableName(typeof(TElement));
            List<int> result = new List<int>();
            foreach (string linkedTable in linkedTables)
            {
                result.Add(base.Database.ExecuteSqlCommand("TRUNCATE TABLE " + linkedTable));
            }
            return result;          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Properties().Where(t => t.Name == "Id" && t.PropertyType == typeof(int))
                .Configure(t => t.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));
            modelBuilder.Entity<FileCollection>()
            .HasOptional(p => p.HeadFileVersion).WithOptionalDependent(t => t.HeadFileVersionInFileCollection).Map(t => t.MapKey("HeadFileVersionId"));
            /*modelBuilder.Entity<FileInstance>()
            .HasOptional(p => p.HeadVersionInFileCollection).WithOptionalPrincipal(p => p.HeadFileVersion);*/
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<FilesLocation> FilesLocations { get; set; }
        public virtual DbSet<Container> Containers { get; set; }
        public virtual DbSet<ClientApplication> ClientApplications { get; set; }
        public virtual DbSet<TemporaryAuthorization> TemporaryAuthorizations { get; set; }
        public virtual DbSet<FileCollection> FileCollections { get; set; }
        public virtual DbSet<FileVersion> FileVersions { get; set; }
        public virtual DbSet<Request> Requests { get; set; }
        public virtual DbSet<FileStatus> FileStatuses { get; set; }
    }
}