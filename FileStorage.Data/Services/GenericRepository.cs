﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FileStorage.Data.Context;

namespace FileStorage.Data.Services
{
    public interface IGenericRepository
    {
        IQueryable<TElement> GetQueryable<TElement>(Expression<Func<TElement, bool>> predicate = null)
            where TElement : class;

        Task<TElement> FindAsync<TElement>(int key, Expression<Func<TElement, bool>> predicate = null)
            where TElement : class;

        int SaveChanges();
        Task<int> SaveChangesAsync();
    }

    public class GenericRepository : IGenericRepository
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        protected FileStorageContext Context { get; private set; }
        public GenericRepository()
        {
            Context = new FileStorageContext();
        }


        public IQueryable<TElement> GetQueryable<TElement>(Expression<Func<TElement, bool>> predicate = null)
            where TElement : class
        {
            IQueryable<TElement> result = Context.Set<TElement>();
            if (predicate != null)
                result = result.Where(predicate);
            return result;
        }

        public async Task<TElement> FindAsync<TElement>(int key, Expression<Func<TElement, bool>> predicate = null)
            where TElement : class
        {
            TElement result = await Context.Set<TElement>().FindAsync(key);
            /*if (predicate != null)
                result = result.Where(predicate);*/
            return result;
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await Context.SaveChangesAsync();
        }

    }
}
