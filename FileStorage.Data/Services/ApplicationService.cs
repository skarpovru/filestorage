﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FileStorage.Data.Context;
using FileStorage.Data.Models;


namespace FileStorage.Data.Services
{
    public interface IApplicationService
       
    {
        IGenericRepository Repository { get; }
        TransactionInformation<ClientApplication> AuthorizeClient(string clientKey);
        TransactionInformation<IQueryable<FileCollection>> GetFileCollections(int clientApplicationId);
        IQueryable<TEntity> Get<TEntity>()
            where TEntity : class;

        Task<TransactionInformation<FileUploadDetail>> CreateFileCollection(NewFileCollectionDTO fileCollection,
            int clientApplicationId);
    }

    public class ApplicationService : IApplicationService
        
    {
        internal static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private IPopulationDataSevice _populationService = new PopulationDataSevice();
        private FileStorageContext _db = new FileStorageContext();

        public IGenericRepository Repository { get; private set; }
        public ApplicationService(IGenericRepository repository)
        {
            Repository = repository;
        }

        public TransactionInformation<ClientApplication> AuthorizeClient(string clientKey)
        {
            TransactionInformation<ClientApplication> result = new TransactionInformation<ClientApplication>();
            if (string.IsNullOrEmpty(clientKey))
            {
                result.Success = false;
                result.StatusCode = HttpStatusCode.Forbidden;
                result.ErrorMessages.Add("Client application key is empty");
                return result;
            }
            ClientApplication client = Repository.GetQueryable<ClientApplication>().FirstOrDefault(t => t.Key == clientKey);
            if (client == null)
            {
                result.Success = false;
                result.StatusCode = HttpStatusCode.Forbidden;
                result.ErrorMessages.Add("Cannot recognize requesting application. Please send correct client application key");
                return result;
            }
            result.Result = client;
            return result;
        }

        public TransactionInformation<IQueryable<FileCollection>> GetFileCollections(int clientApplicationId)
        {
            TransactionInformation<IQueryable<FileCollection>> result = new TransactionInformation<IQueryable<FileCollection>>();
            if (clientApplicationId == 0)
            {
                result.Success = false;
                result.StatusCode = HttpStatusCode.Forbidden;
                result.ErrorMessages.Add("Cannot recognize requesting application. Please send correct application id");
                return result;
            }

            result.Result =
                Repository.GetQueryable<FileCollection>().Where(t => t.ClientApplicationId == clientApplicationId);

            return result;
        }
        public IQueryable<TEntity> Get<TEntity>()
            where TEntity : class
        {
            return Repository.GetQueryable<TEntity>();
        }

        public async Task<TransactionInformation<FileUploadDetail>> CreateFileCollection(NewFileCollectionDTO fileCollection, int clientApplicationId)
        {
            TransactionInformation<FileUploadDetail> result = new TransactionInformation<FileUploadDetail>();
            int serverId;
            Server server;
            if (fileCollection.PreferableServerId == null)
            {
                //Get default server for user based on user location
                //Get user location
                string countryCode = _populationService.GetEmployeeCountryCode(fileCollection.Employee.EmployeeId);
                TransactionInformation<Server> getServerForLocation = GetServerForLocation(countryCode);
                if (!getServerForLocation.Success)
                {
                    result.Success = false;
                    result.StatusCode = getServerForLocation.StatusCode;
                    result.ErrorMessages.AddRange(getServerForLocation.ErrorMessages);
                    return result;
                }
                server = getServerForLocation.Result;
                serverId = server.Id;
            }
            else
            {
                serverId = (int)fileCollection.PreferableServerId;
                server = _db.Servers.FirstOrDefault(t => t.Id == serverId && t.OpenForWrite && t.Available);
                if (server == null)
                {
                    result.Success = false;
                    result.StatusCode = HttpStatusCode.BadRequest;
                    result.ErrorMessages.Add("Requested server Id "+serverId+" not found or not available for write operation");
                    return result;
                }
            }
            FilesLocation filesLocation = _db.FilesLocations.FirstOrDefault(t => t.ServerId == serverId && t.OpenForWrite && t.Available);
            if (filesLocation == null)
            {
                result.Success = false;
                result.ErrorMessages.Add("Cannot find available file location for write on the server Id " + serverId);
                return result;
            }
            int filesLocationId = filesLocation.Id;
            Container container =
                _db.Containers.FirstOrDefault(t => t.FilesLocationId == filesLocationId && t.ClientApplicationId == clientApplicationId && t.OpenForWrite && t.Available);
            if (container == null)
            {
                //Create new container
                Random random = new Random();
                int randomNumber = random.Next(900000, 999999);
                container = new Container
                {
                    Name = randomNumber.ToString(),
                    OpenForWrite = true,
                    Available = true,
                    FilesLocationId = filesLocationId,
                    ClientApplicationId = clientApplicationId
                };
                _db.Containers.Add(container);
                await _db.SaveChangesAsync();
                //Set name from Id (should be set in database)
                container.Name = container.Id.ToString();
            }
            //int containerId = container.Id;

            FileCollection newFileCollection = new FileCollection
            {
                Name = fileCollection.Name,
                Description = fileCollection.Description,
                SupportVersioning = fileCollection.SupportVersioning == null || (bool)fileCollection.SupportVersioning,
                Archivable = fileCollection.Archivable == null || (bool)fileCollection.Archivable,
                ExpireInDays = fileCollection.ExpireInDays ?? 0,
                ClientApplicationId = clientApplicationId
            };

            FileStatus fileStatusForUploading = _db.FileStatuses.FirstOrDefault(t => t.NotLoaded);
            if (fileStatusForUploading == null)
            {
                result.Success = false;
                result.ErrorMessages.Add("Cannot find default NotLoaded status for a file");
                return result;
            }
            int fileStatusForUploadingId = fileStatusForUploading.Id;

            FileVersion newFileVersion = new FileVersion
            {
                Id = Guid.NewGuid(),
                Name = fileCollection.HeadFileVersion.Name,
                Extension = fileCollection.HeadFileVersion.Extension,
                VersionDescription = fileCollection.HeadFileVersion.VersionDescription,
                Version = 1,
                Size = fileCollection.HeadFileVersion.Size,
                Hash = fileCollection.HeadFileVersion.Hash,
                Container = container,
                ExpiredIfNotLoadedAt = DateTimeOffset.UtcNow.AddMinutes(30),
                FileCollection = newFileCollection,
                StatusId = fileStatusForUploadingId,
                Valid = true
            };

            _db.FileCollections.Add(newFileCollection);
            _db.FileVersions.Add(newFileVersion);
            await _db.SaveChangesAsync();
            newFileCollection.HeadFileVersion = newFileVersion;
            await _db.SaveChangesAsync();

            //Create upload request
            Request ruquest = new Request
            {
                FileVersion = newFileVersion,
                RequestedAt = DateTimeOffset.UtcNow,
                RequestedBy = fileCollection.Employee.EmployeeId,
                ToWrite = true
            };
            _db.Requests.Add(ruquest);
            TemporaryAuthorization temporaryAuthorization = new TemporaryAuthorization
            {
                FileVersion = newFileVersion,
                ToWrite = true,
                Login = fileCollection.Employee.Login,
                ExpireAt = DateTimeOffset.UtcNow.AddMinutes(30)
            };
            _db.TemporaryAuthorizations.Add(temporaryAuthorization);
            await _db.SaveChangesAsync();


            //Return file id and node url for upload request
            result.Result = new FileUploadDetail
            {
                FileId = newFileVersion.Id,
                Url = server.ClientUrl
            };

            return result;
        }

        private TransactionInformation<Server> GetServerForLocation(string countryCode)
        {
            TransactionInformation<Server> result = new TransactionInformation<Server>();
            try
            {
                Server server =
                _db.Servers.FirstOrDefault(t => t.CountryCode == countryCode && t.OpenForWrite && t.Available);
                if (server == null)
                {
                    //No active servers for required country
                    //Set default 
                    server =
                    _db.Servers.FirstOrDefault(t => t.OpenForWrite && t.Available);
                    if (server == null)
                    {
                        result.Success = false;
                        result.ErrorMessages.Add("Cannot find any servers for new files");
                        return result;
                    }
                }
                result.Result = server;
            }
            catch (Exception ex)
            {
                Logger.Error("Error: " + ex);
                result.Success = false;
                result.ErrorMessages.Add(ex.Message);
            }
            return result;
        }
    }
}
