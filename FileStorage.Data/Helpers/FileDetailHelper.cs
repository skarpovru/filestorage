﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileStorage.Data.Models;

namespace FileStorage.Data.Helpers
{
    public static class FileHelper
    {
        public static string FullPathCalculator(FileLocationDetailResponse fileLocationDetail, Guid fileId)
        {
            return fileLocationDetail.Path + "\\" + fileLocationDetail.ContainerName + "\\" + fileId + "." + fileLocationDetail.FileExtension;
        }
    }
}
